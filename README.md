# DB SETUP

## to RUN Postgresql Docker
```
cd db && docker-compose up --build -d
```

## to Stop Postgresql Docker
```
cd db && docker-compose down
```

### For installing docker
See [Configuration Reference](https://docs.docker.com/desktop/install/windows-install/).

### For installing WSL
See [Configuration Reference](https://learn.microsoft.com/en-us/windows/wsl/install).

