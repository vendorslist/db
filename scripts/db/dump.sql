-- CreateTable
-- CREATE TABLE IF NOT EXISTS users (
--   "id" SERIAL PRIMARY KEY,
--   "username" TEXT NOT NULL,
--   "password" TEXT NOT NULL
-- );

-- CREATE TABLE IF NOT EXISTS vendors (
--   "id" SERIAL PRIMARY KEY,
--   "name" TEXT NOT NULL,
--   "user_id" integer
-- );

-- ALTER TABLE vendors
--       ADD CONSTRAINT fk_vendor_users FOREIGN KEY (user_id) 
--           REFERENCES users(id);

-- Seed
-- INSERT INTO users (name, password) VALUES ('John', '12345');
-- INSERT INTO vendors (name, user_id) VALUES ('John', 1);
